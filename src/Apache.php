<?php

namespace kerasai\torch;

use Symfony\Component\Process\Process;

class Apache {

  /**
   * @param string $name
   * @return $this
   */
  public function addVhost($name) {
    $loader = new \Twig_Loader_Filesystem('templates');
    $twig = new \Twig_Environment($loader);
    $vars = [
      'server_name' => $name . '.local',
      'docroot' => '/sites/' . $name . '/web',
      'ssl_cert_file' => '/config/' . $name . '/certs/' . $name . '-cert.pem',
      'ssl_cert_key_file' => '/config/' . $name . '/certs/' . $name . '-key.pem',
    ];
    $output = $twig->render('apache.vhost.twig', $vars);
    $file = '/config/' . $name . '/vhost.conf';
    file_put_contents($file, $output);
    return $this;
  }

  /**
   * @param string $name
   * @return $this
   */
  public function enSite($name) {
    $this->disSite($name);
    $source = '/config/' . $name . '/vhost.conf';
    $dest = '/etc/apache2/sites-enabled/' . $name . '.conf';
    symlink($source, $dest);
    return $this;
  }

  /**
   * @param string $name
   * @return $this
   */
  public function disSite($name) {
    $file = '/etc/apache2/sites-enabled/' . $name . '.conf';
    if (file_exists($file)) {
      unlink($file);
    }
    return $this;
  }

  /**
   * @return $this
   */
  public function reload() {
    $command = ['service', 'apache2', 'reload'];
    $process = new Process($command);
    $process->mustRun();
    return $this;
  }

  /**
   * @return $this
   */
  public function restart() {
    $command = ['service', 'apache2', 'restart'];
    $process = new Process($command);
    $process->mustRun();
    return $this;
  }

}
