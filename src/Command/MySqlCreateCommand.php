<?php

namespace kerasai\torch\Command;

use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class MySqlCreateCommand extends CommandBase {

  protected function configure() {
    $this->setName('mysql:create')
      ->setDescription('Creates a MySQL database and user.')
      ->addArgument('name', InputArgument::REQUIRED, 'The name of the database and user to create.');
  }

  protected function execute(InputInterface $input, OutputInterface $output) {
    $name = $input->getArgument('name');
    $this->getMySql()->createDb($name);
    $output->writeln('Done.');
  }

  /**
   * @return \kerasai\torch\MySql
   */
  protected function getMySql() {
    return $this->container->get('mysql');
  }
}
