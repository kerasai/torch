<?php

namespace kerasai\torch\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

abstract class CommandBase extends Command implements ContainerAwareInterface {

  use ContainerAwareTrait;

}
