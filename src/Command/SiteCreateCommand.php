<?php

namespace kerasai\torch\Command;

use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class SiteCreateCommand extends CommandBase {

  protected function configure() {
    $this->setName('site:create')
      ->setDescription('Creates a site.')
      ->addArgument('name', InputArgument::REQUIRED, 'The name of the site to create.');
  }

  protected function execute(InputInterface $input, OutputInterface $output) {
    $name = $input->getArgument('name');
    $this->getSiteManager()->createSite($name);
    $output->writeln('Done.');
  }

  /**
   * @return \kerasai\torch\SiteManager
   */
  protected function getSiteManager() {
    return $this->container->get('site_manager');
  }

}
