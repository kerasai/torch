<?php

namespace kerasai\torch\Command;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class InitCommand extends CommandBase {

  protected function configure() {
    $this->setName('init')
      ->setDescription('Initializes Torch.');
  }

  protected function execute(InputInterface $input, OutputInterface $output) {
    $this->getMySql()->createAdminUser();
    $output->writeln('Done.');
  }

  /**
   * @return \kerasai\torch\MySql
   */
  protected function getMySql() {
    return $this->container->get('mysql');
  }

  /**
   * @return \kerasai\torch\Config
   */
  protected function getConfig() {
    return $this->container->get('config');
  }

}
