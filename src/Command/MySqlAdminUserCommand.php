<?php

namespace kerasai\torch\Command;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class MySqlAdminUserCommand extends CommandBase {

  protected function configure() {
    $this->setName('mysql:admin-user')
      ->setDescription('Initializes MySQL admin user.');
  }

  protected function execute(InputInterface $input, OutputInterface $output) {
    $this->getMySql()->createAdminUser();
    $output->writeln('Done.');
  }

  /**
   * @return \kerasai\torch\MySql
   */
  protected function getMySql() {
    return $this->container->get('mysql');
  }
}
