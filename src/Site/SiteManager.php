<?php

namespace kerasai\torch\Site;

use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

class SiteManager implements ContainerAwareInterface {

  use ContainerAwareTrait;

  const SITES_DIR = '/sites';

  public function createSite($name) {
    $this->getSsl()->createCert($name);
    $this->getMySql()->createDb($name);

    $path = '/sites/' . $name . '/web';
    if (!is_readable($path)) {
      mkdir($path, 0755, TRUE);
    }
    $this->getApache()->addVhost($name)->enSite($name)->reload();
  }

  /**
   * @return \kerasai\torch\Apache
   */
  protected function getApache() {
    return $this->container->get('apache');
  }

  /**
   * @return \kerasai\torch\MySql
   */
  protected function getMySql() {
    return $this->container->get('mysql');
  }

  /**
   * @return \kerasai\torch\Ssl
   */
  protected function getSsl() {
    return $this->container->get('ssl');
  }

}
