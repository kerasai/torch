<?php

namespace kerasai\torch;

use PDO;
use Symfony\Component\Process\Process;

class MySql {

  public function createAdminUser() {
    $sql[] = "CREATE USER IF NOT EXISTS 'torch'@'%' IDENTIFIED BY 'torch'";
    $sql[] = "GRANT ALL PRIVILEGES ON *.* TO 'torch'@'%' WITH GRANT OPTION";
    $sql[] = "FLUSH PRIVILEGES";
    $sql = implode(';', $sql);

    $command = ['mysql', '-u', 'root', '-e', $sql];
    $process = new Process($command);
    $process->mustRun();
  }

  public function createDb($name) {
    $pdo = new PDO("mysql:host=localhost", 'torch', 'torch');
    $pdo->exec("CREATE USER '$name'@'%' IDENTIFIED BY '$name'");
    $pdo->exec("GRANT USAGE ON *.* TO '$name'@'%' IDENTIFIED BY '$name' REQUIRE NONE WITH MAX_QUERIES_PER_HOUR 0 MAX_CONNECTIONS_PER_HOUR 0 MAX_UPDATES_PER_HOUR 0 MAX_USER_CONNECTIONS 0");
    $pdo->exec("CREATE DATABASE IF NOT EXISTS `$name`");
    $pdo->exec("GRANT ALL PRIVILEGES ON `$name`.* TO '$name'@'%'");
    $pdo->exec("FLUSH PRIVILEGES");
  }

}
