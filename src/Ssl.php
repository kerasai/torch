<?php

namespace kerasai\torch;

use Symfony\Component\Process\Process;

class Ssl {

  public function createCert($name) {
    $certDir = '/config/' . $name . '/certs';
    @mkdir($certDir, 0777, TRUE);

    // -subj "/C=PE/ST=Lima/L=Lima/O=Acme Inc. /OU=IT Department/CN=acme.com"

    $command[] = 'openssl';
    $command[] = 'req';
    $command[] = '-x509';
    $command[] = '-nodes';
    $command[] = '-days';
    $command[] = '7300';
    $command[] = '-newkey';
    $command[] = 'rsa:2048';
    $command[] = '-keyout';
    $command[] = $certDir . '/' . $name . '-key.pem';
    $command[] = '-out';
    $command[] = $certDir . '/' . $name . '-cert.pem';

    $command[] = '-subj';
    $command[] = '/CN=' . $name . '.local';

    $process = new Process($command);
    $process->setWorkingDirectory($certDir);
    $process->mustRun();
  }

}
